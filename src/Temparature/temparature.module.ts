import { Module } from '@nestjs/common';
import { TemparatureController } from './temparature.controller';
import { TemparatureService } from './temparature.service';

@Module({
  imports: [],
  controllers: [TemparatureController],
  providers: [TemparatureService],
  exports: [],
})
export class TemparatureModule {}
