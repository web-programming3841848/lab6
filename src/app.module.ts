import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TemparatureModule } from './Temparature/temparature.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [TemparatureModule, UsersModule],
  controllers: [AppController],
  providers: [AppService],
  exports: [],
})
export class AppModule {}
